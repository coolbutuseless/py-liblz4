Python CFFI Bindings for lz4
============================

This is a python [cffi](http://cffi.readthedocs.org/en/release-0.8/#struct-array-example) binding for [https://code.google.com/p/lz4/](lz4).

> LZ4 is a very fast lossless compression algorithm, providing compression speed at 400 MB/s per core, with near-linear scalability for multi-threaded applications. It also features an extremely fast decoder, with speed in multiple GB/s per core, typically reaching RAM speed limits on multi-core systems. 

This wrapper was tested with `lz4-r127` on OSX.

Compiling lz4 as a shared lib
=============================
To use lz4 with python/cffi, it must be compiled as a shared library.

1. Download [lz4-r127 src](https://github.com/Cyan4973/lz4/releases/latest) from github
2. Copy `lz4.c` and `lz4.h` into this directory.
3. Type `make`

This has only been tested on OSX and only with **r127** of lz4.

Example
=======
* See `example_comp_decomp.py`

Modifed Header
==============
* CFFI only has limited support for *#define*s which means `lz4.h` can't be just used as-is.
* Instead, `lz4.h` has been copied to `liblz4.h` and tidied up to be CFFI compatible.
