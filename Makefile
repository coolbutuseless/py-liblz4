all: lz4.o
	gcc -shared -o liblz4.so.1.0 lz4.o

OPTS := -I. -std=c99 -Ofast -Wall -Wextra -Wundef -Wshadow -Wstrict-prototypes -fPIC

xxhash.o: xxhash.c
	gcc $(OPTS) -c lz4.c

clean:
	rm *.o *.so.*
