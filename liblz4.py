#!/usr/bin/env python
from __future__ import print_function
import os

from cffi import FFI
ffi = FFI()

this_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
with open(this_dir + "liblz4.h") as header:
    ffi.cdef(header.read())
liblz4 = ffi.dlopen(this_dir + "liblz4.so.1.0")
