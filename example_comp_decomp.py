#!/usr/bin/env python
from __future__ import print_function

from liblz4 import liblz4

from cffi import FFI
ffi = FFI()

instring = b"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris. Maecenas congue ligula ac quam viverra nec consectetur ante hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur. Donec ut libero sed arcu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor. Pellentesque auctor nisi id magna consequat sagittis. Curabitur dapibus enim sit amet elit pharetra tincidunt feugiat nisl imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros. Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est."

# get an estimate of compressed size maximum
maxsize = liblz4.LZ4_compressBound(len(instring))
print("Need to allocation %i bytes in the compressed_buffer for the worst case" % maxsize)

# Allocate some space for the output
compressed_buffer = ffi.new("char[]", maxsize)
decompressed_buffer = ffi.new("char[]", len(instring))

compressed_size = liblz4.LZ4_compress(instring, compressed_buffer, len(instring))
print("input size :", len(instring))
print("compressed size:", compressed_size)

# Decompression
err = liblz4.LZ4_decompress_safe(compressed_buffer, decompressed_buffer, compressed_size, len(decompressed_buffer))
print("Decompression status:", err)
assert err > 0

print(ffi.string(decompressed_buffer))

